package com.hzmg.strategyfactory;

import com.hzmg.strategyfactory.factory.PetStrategyServiceFactory;
import com.hzmg.strategyfactory.pojo.Parameter;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class StrategyFactoryDemoApplicationTests {
    @Autowired
    PetStrategyServiceFactory strategyServiceFactory;
    @Test
    void contextLoads() {
        System.out.println(strategyServiceFactory.nameForThings(new Parameter("pig","pigType")));
    }

}
