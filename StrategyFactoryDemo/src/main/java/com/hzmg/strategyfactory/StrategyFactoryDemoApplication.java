package com.hzmg.strategyfactory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StrategyFactoryDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(StrategyFactoryDemoApplication.class, args);
    }

}
