package com.hzmg.strategyfactory.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Parameter {
    String name;
    String type;
}
