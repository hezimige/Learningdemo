package com.hzmg.strategyfactory.service;

import com.hzmg.strategyfactory.pojo.Parameter;

/**
 * @author zbw
 */
public interface PetStrategyService {
    /**
     * 返回结果
     */
    String nameForThings(Parameter dto);

    /**
     * 返回类型值
     */
    String getType();
}
