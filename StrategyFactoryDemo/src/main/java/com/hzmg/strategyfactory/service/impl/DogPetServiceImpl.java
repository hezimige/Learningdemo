package com.hzmg.strategyfactory.service.impl;

import com.hzmg.strategyfactory.pojo.Parameter;
import com.hzmg.strategyfactory.service.PetStrategyService;
import org.springframework.stereotype.Service;

/**
 * @author zbw
 */
@Service
public class DogPetServiceImpl implements PetStrategyService {
    @Override
    public String nameForThings(Parameter dto){
        return dto.getName()+" true is dog";
    }
    @Override
    public String getType(){
        return "dogType";
    }

}
