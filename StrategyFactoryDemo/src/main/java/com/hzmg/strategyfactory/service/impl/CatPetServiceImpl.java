package com.hzmg.strategyfactory.service.impl;

import com.hzmg.strategyfactory.pojo.Parameter;
import com.hzmg.strategyfactory.service.PetStrategyService;
import org.springframework.stereotype.Service;

/**
 * @author zbw
 */
@Service
public class CatPetServiceImpl implements PetStrategyService {
    @Override
    public String nameForThings(Parameter dto) {
        return dto.getName()+" true is cat";
    }

    @Override
    public String getType() {
        return "catType";
    }
}
