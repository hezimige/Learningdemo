package com.hzmg.strategyfactory.service.impl;

import com.hzmg.strategyfactory.pojo.Parameter;
import com.hzmg.strategyfactory.service.PetStrategyService;
import org.springframework.stereotype.Service;

@Service
public class PigPetServiceImpl implements PetStrategyService {
    @Override
    public String nameForThings(Parameter dto) {
        return dto.getName()+" true is pig";
    }

    @Override
    public String getType() {
        return "pigType";
    }
}
