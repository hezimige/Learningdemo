package com.hzmg.common;

import com.google.common.collect.Lists;
import com.hzmg.common.common.StoreStrategy;
import lombok.Data;

import java.util.List;

/**
 * Akka-worker's configuration
 *
 * @author zbw
 * @date 2022/6/27
 */
@Data
public class AkkaWorkerConfig {
    /**
     * AppName,recommend to use the name of this project
     * I think that application should be registered by itself
     */
    private System appName;
    /**
     * Worker port
     * Random port is enabled when
     */
    private int port = 277777;
    /**
     * Address of Akka-server node(s)
     * Do not mistake for ActorSystem port. Do not add any prefix, i.e. http://.
     * Example ,172.17.147.118:8080
     */
    private List<String> serverAddress = Lists.newArrayList();
    /**
     * Max length of response result. Result that is longer than the value will be truncated.
     * {@link ProcessResult} max length for #msg
     */
    private int maxResultLength = 8096;
    /**
     * User-defined context object,which is passed through to the TaskContext#userContext property
     * Usage Scenarios: The container Java processor needs to use the Spring bean of the host application, where you can pass in the ApplicationContext and get the bean in the Processor.
     */
    private Object userContext;
    /**
     * Internal persistence method, DISK or MEMORY
     * Normally you don't need to care about this configuration
     */
    private StoreStrategy storeStrategy = StoreStrategy.DISK;
    /**
     * If test mode is set as true.Akka-worker no longer connects to the server or validates appName.
     * Test mode is used for conditions that you have no akka-server in your develop env so you can't startup the application
     */
    private boolean enableTestMode = false;
    /**
     * Max length of appended workflow context value length.Appended workflow context value that is longer than the value will be ignore.
     * {@link WorkflowContext} max length for #appendedContextData
     */
    private int maxAppendedWfContextLength = 8192;

}
