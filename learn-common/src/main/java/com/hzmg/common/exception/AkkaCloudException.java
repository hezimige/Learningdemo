package com.hzmg.common.exception;

/**
 * AkkaCloud 运行时异常
 *
 * @author zbw
 * @since 2022/7/6
 */
public class AkkaCloudException extends RuntimeException {

    public AkkaCloudException() {
    }

    public AkkaCloudException(String message) {
        super(message);
    }

    public AkkaCloudException(String message, Throwable cause) {
        super(message, cause);
    }

    public AkkaCloudException(Throwable cause) {
        super(cause);
    }

    public AkkaCloudException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
