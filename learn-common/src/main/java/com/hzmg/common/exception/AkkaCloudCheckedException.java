package com.hzmg.common.exception;

/**
 * AkkaCloud 受检异常，需要开发者手动处理
 *
 * @author zbw
 * @since 2022/7/6
 */
public class AkkaCloudCheckedException extends Exception {

    public AkkaCloudCheckedException() {
    }

    public AkkaCloudCheckedException(String message) {
        super(message);
    }

    public AkkaCloudCheckedException(String message, Throwable cause) {
        super(message, cause);
    }

    public AkkaCloudCheckedException(Throwable cause) {
        super(cause);
    }

    public AkkaCloudCheckedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
