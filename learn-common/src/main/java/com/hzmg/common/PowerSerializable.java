package com.hzmg.common;

import java.io.Serializable;

/**
 * AkkaDemo serializable interface.
 *
 * @author zbw
 * @since 2022/6/23
 */
public interface PowerSerializable extends Serializable {

    /**
     * request path for http or other protocol, like '/worker/stopInstance'
     * @return null for non-http request object or no-null path for http request needed object
     */
    default String path() {
        return null;
    }
}
