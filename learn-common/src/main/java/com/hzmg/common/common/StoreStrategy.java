package com.hzmg.common.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 持久化策略
 * @author zbw
 * @since 2022/6/28
 */
@Getter
@AllArgsConstructor
public enum StoreStrategy {
    DISK("磁盘"),
    MEMORY("内存");
    private final String des;
}
