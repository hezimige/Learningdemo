package com.hzmg.netty;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.CharsetUtil;

/**
 * netty提供的异步处理队列
 */
public class TaskQueueDemo extends ChannelInboundHandlerAdapter {
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        //获取到线程池的eventLoop，添加线程并执行
        ctx.channel().eventLoop().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    //在这里将长时间的操作进行异步处理，这里用休眠来体现
                    System.out.println("[TaskQueueDemo]:静候一秒");
                    Thread.sleep(1000);
                    //获取客户端发送过来的消息
                    ByteBuf byteBuf = (ByteBuf) msg;
                    System.out.println("[TaskQueueDemo]:收到客户端" + ctx.channel().remoteAddress() + "发送的消息: " + byteBuf.toString(CharsetUtil.UTF_8));
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        //异常捕获，以及对应处理，这里的处理为关闭通道
        ctx.close();
    }
}
