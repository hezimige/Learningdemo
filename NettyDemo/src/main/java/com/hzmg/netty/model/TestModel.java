package com.hzmg.netty.model;

import lombok.Data;

import java.io.Serializable;

/**
 * ObjectDemo
 * @author zbw
 */
@Data
public class TestModel implements Serializable {
    private String name;
    private int id;
}
