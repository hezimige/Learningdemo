package com.hzmg.vertxweb;


import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

/**
 * simpleApplication
 * @author zbw
 */
@SpringBootApplication
public class SimpleApplication {
    public static void main(String[] args) {
        new SpringApplicationBuilder(SimpleApplication.class)
                .web(WebApplicationType.NONE)
                .run(args);
    }
}
