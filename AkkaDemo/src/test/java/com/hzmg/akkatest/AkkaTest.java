package com.hzmg.akkatest;

import cn.hutool.core.math.MathUtil;
import com.hzmg.akka.ActorSystemDemo;
import com.hzmg.akka.SimpleApplication;
import lombok.AllArgsConstructor;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = SimpleApplication.class)
@RunWith(SpringRunner.class)
public class AkkaTest {
    @Autowired
    private ActorSystemDemo actorSystemDemo;
    @Test
    public void test1(){
        try {
            actorSystemDemo.createActorSystem();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Test
    public void test2(){
        ListNode head=new ListNode(1);
        ListNode head2=new ListNode(2);
        ListNode head3=new ListNode(3);
        ListNode head4=new ListNode(1);
        head.next=head2;
        head2.next=head3;
        head3.next=head4;
        System.out.println(isPail(head));
    }
    public boolean isPail (ListNode head) {
        if(head==null||head.next==null){
            return true;
        }
        ListNode cur=head;
        ListNode second=reverseList(cur);
        while(head!=null){
            if(second.val!=head.val){
                return false;
            }
            System.out.print("second:"+second.val+" head:"+head.val);

            head=head.next;
            second=second.next;
        }
        return true;
        // write code here
    }
    public ListNode reverseList(ListNode head){
        ListNode pre=null;
        ListNode cur=head;
        while(cur!=null){
            ListNode next=cur.next;
            cur.next=pre;
            pre=cur;
            cur=next;
        }
        return pre;
    }

    public class ListNode {
    int val;
    ListNode next = null;
    public ListNode(int val){
        this.val=val;
    }
 }
    @Test
    public void test3(){
        ListNode head=new ListNode(1);
        ListNode head2=new ListNode(4);
        ListNode head3=new ListNode(6);
        ListNode head4=new ListNode(3);
        ListNode head5=new ListNode(7);
        head.next=head2;
        head2.next=head3;
        head3.next=head4;
        head4.next=head5;
        System.out.println(oddEvenList(head));
    }
    public ListNode oddEvenList (ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode left =  new ListNode(head.val), right = new ListNode(head.next.val);
        ListNode cur1 = left, cur2 = right;
        ListNode one = head, two = head.next;
        while ( one != null&&two != null &&one.next!=null&&two.next!=null) {

            one = one.next.next;
            two = two.next.next;
            if (one != null) {
                cur1.next = one;
                cur1 = cur1.next;
            }
            if (two != null) {
                cur2.next = two;
                cur2 = cur2.next;
            }

        }
        cur1.next = right;
        return left;
        // write code here
    }
}
