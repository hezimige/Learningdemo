package com.hzmg.akka.extension;

import com.hzmg.common.model.SystemMetrics;

/**
 * user-customized system metrics collector
 *
 * @author zbw
 * @since 2022/6/29
 */
public interface SystemMetricsCollector {

    /**
     * SystemMetrics, you can put your custom metrics info in the 'extra' param
     * @return SystemMetrics
     */
    SystemMetrics collect();
}
