package com.hzmg.akka.background;

import com.google.common.collect.Maps;
import com.hzmg.akka.utils.CommonUtils;
import com.hzmg.akka.utils.HttpUtils;
import com.hzmg.common.AkkaWorkerConfig;
import com.hzmg.common.exception.AkkaCloudException;
import com.hzmg.common.response.ResultDTO;
import com.hzmg.common.serialize.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 服务发现
 *
 * @author zbw
 * @date 2022/6/27
 */
@Slf4j

public class ServerDiscoveryService {
    //服务发现地址
    //http://%s/server/acquire?appId=%d&currentServer=%s&protocol=AKKA
    private static final String DISCOVERY_URL = "http://%s/server/acquire?appId=%d&currentServer=%s&protocol=AKKA";
    // 最大失败次数
    private static final int MAX_FAILED_COUNT = 3;
    // 失败次数
    private static int FAILED_COUNT = 0;
    private final Long appId;
    private final AkkaWorkerConfig config;
    private final Map<String, String> ip2Address = Maps.newHashMap();
    private String currentServerAddress;

    public ServerDiscoveryService(Long appId, AkkaWorkerConfig config) {
        this.appId = appId;
        this.config = config;
    }

    public void start(ScheduledExecutorService timingPool) {
        this.currentServerAddress = discovery();
        if (StringUtils.isEmpty(this.currentServerAddress) && !config.isEnableTestMode()) {
            throw new AkkaCloudException("can't find any available server, this worker has been quarantined.");
        }
        //以固定频率方式调用该方法，初始延迟10秒，每十秒执行一次。
        timingPool.scheduleAtFixedRate(() -> this.currentServerAddress = discovery(), 10, 10, TimeUnit.SECONDS);
    }

    public String getCurrentServerAddress() {
        return currentServerAddress;
    }

    private String discovery() {
        if (ip2Address.isEmpty()) {
            //split(":"): Exclude the port
            config.getServerAddress().forEach(x -> ip2Address.put(x.split(":")[0], x));
        }
        String result = null;
        //先对当前机器发起请求
        String currentServer = currentServerAddress;
        if (!StringUtils.isEmpty(currentServer)) {
            String ip = currentServer.split(":")[0];
            //请求当前Server的Http服务
            String firstServerAddress = ip2Address.get(ip);
            if (firstServerAddress != null) {
                result = acquire(firstServerAddress);
            }
        }

        for (String httpServerAddress : config.getServerAddress()) {
            if (StringUtils.isEmpty(result)) {
                result = acquire(httpServerAddress);
            } else {
                break;
            }
        }
        if (StringUtils.isEmpty(result)) {
            log.warn("[AkkaCloudDiscovery] can't find any available console, this worker has been quarantined.");

            // 在 Server 高可用的前提下，连续失败多次，说明该节点与外界失联，Server已经将秒级任务转移到其他Worker，需要杀死本地的任务
           /* if (FAILED_COUNT++ > MAX_FAILED_COUNT) {

                log.warn("[AkkaCloudDiscovery] can't find any available console for 3 consecutive times, It's time to kill all frequent job in this server.");
                List<Long> frequentInstanceIds = TaskTrackerPool.getAllFrequentTaskTrackerKeys();
                if (!CollectionUtils.isEmpty(frequentInstanceIds)) {
                    frequentInstanceIds.forEach(instanceId -> {
                        TaskTracker taskTracker = TaskTrackerPool.remove(instanceId);
                        taskTracker.destroy();
                        log.warn("[PowerDiscovery] kill frequent instance(instanceId={}) due to can't find any available server.", instanceId);
                    });
                }

                FAILED_COUNT = 0;
            }*/
            return null;
        } else {
            // 重置失败次数
            FAILED_COUNT = 0;
            log.debug("[PowerDiscovery] current server is {}.", result);
            return result;
        }
    }

    @SuppressWarnings("rawtypes")
    /**
     * 查看请求的服务是否能正常访问
     */
    private String acquire(String httpServerAddress) {
        String result = null;
        String url = String.format(DISCOVERY_URL, httpServerAddress, appId, currentServerAddress);
        try {
            result = CommonUtils.executeWithRetry0(() -> HttpUtils.get(url));
        } catch (Exception ignore) {

        }
        if (!StringUtils.isEmpty(result)) {
            try {
                ResultDTO resultDTO = JsonUtils.parseObject(result, ResultDTO.class);
                if (resultDTO.isSuccess()) {
                    return resultDTO.getData().toString();
                }
            } catch (Exception ignore) {

            }
        }
        return null;
    }
}
