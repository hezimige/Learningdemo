package com.hzmg.akka.Aspect;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * remote service class
 */
@Component
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RemoteServiceClass{}
