package com.hzmg.akka.Aspect;

import akka.actor.ActorSystem;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * service remote call
 * @author zbw
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RemoteServiceCall {

    String name();

    /**
     * to control the remote way,web or actor.
     */
    boolean isWeb() default false;

    /**
     * the type of interface ,eg:get,post
     */
    String type() default "get";
}
