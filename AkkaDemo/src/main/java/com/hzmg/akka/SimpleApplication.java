package com.hzmg.akka;

import akka.pattern.Patterns;
import com.hzmg.akka.service.RemoteCallServiceDemo1;
import com.hzmg.akka.utils.RemoteCallUtils;
import com.hzmg.akka.utils.SpringUtils;
import com.hzmg.common.exception.AkkaCloudException;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import java.lang.reflect.Method;
import java.time.Duration;
import java.util.Arrays;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;

/**
 * @author zbw
 * @date 2022/6/23
 */
@Slf4j
@SpringBootApplication
public class SimpleApplication {
    private static Logger logger = LoggerFactory.getLogger(SimpleApplication.class);

    public static void main(String[] args) {
        new SpringApplicationBuilder(SimpleApplication.class)
                .web(WebApplicationType.NONE)
                .run(args);
        //String result = (String) RemoteCallUtils.getRemoteService("hi remote server", "helloRemote", "remote-call");
        //异步非阻塞调用示例

       /* CompletionStage<Object> askCS = RemoteCallUtils.getRemoteServiceNio("hi remote server", "helloRemote", "remote-call");
        try {
              askCS.whenComplete((response,throwable)->{
                try{
                    System.out.println(response.toString());
                }catch (Exception e){
                    System.out.println(throwable.toString());
                }
            });
        } catch (Exception e) {
            throw new AkkaCloudException(e.toString());
        }*/
        logger.info("this is the end");
    }
}
