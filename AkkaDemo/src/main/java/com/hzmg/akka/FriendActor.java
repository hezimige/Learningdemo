package com.hzmg.akka;

import akka.actor.AbstractActor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
public class FriendActor extends AbstractActor {
    @Override
    public Receive createReceive() {

        return receiveBuilder()
                .match(String.class,this::onReceivePing)
                .matchAny(obj->log.warn("unknow request:{}.",obj))
                .build();
    }
    private void onReceivePing(String msg) {
        getSender().tell(msg, getSelf());
        log.info("通信接受成功，接受信息为：{}",msg);
    }
}
