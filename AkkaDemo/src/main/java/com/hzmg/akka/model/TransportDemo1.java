package com.hzmg.akka.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * first transport demo
 * @author zbw
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransportDemo1 implements Serializable  {
    int id;
    String name;
}
