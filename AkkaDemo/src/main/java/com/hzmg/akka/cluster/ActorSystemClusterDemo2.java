package com.hzmg.akka.cluster;

import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.cluster.routing.ClusterRouterGroup;
import akka.cluster.routing.ClusterRouterGroupSettings;
import akka.pattern.Patterns;
import akka.routing.ConsistentHashingGroup;
import com.google.common.collect.Maps;
import com.hzmg.akka.FriendActor;
import com.hzmg.akka.utils.NetUtils;
import com.hzmg.common.exception.AkkaCloudException;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class ActorSystemClusterDemo2 {
    public static void main(String[] args) {
        ActorSystemClusterDemo1.createActorSystem("1001","akka-server");
    }
}
