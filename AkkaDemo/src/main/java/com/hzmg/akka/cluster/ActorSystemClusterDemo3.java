package com.hzmg.akka.cluster;

import akka.actor.ActorSystem;
import akka.actor.Props;
import com.google.common.collect.Maps;
import com.hzmg.akka.FriendActor;
import com.hzmg.akka.utils.NetUtils;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;

@Slf4j
@Component
public class ActorSystemClusterDemo3 {
    public static void main(String[] args) {
        ActorSystemClusterDemo1.createActorSystem("1002","akka-server");
    }


}
