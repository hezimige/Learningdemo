package com.hzmg.akka;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * mvc with akka http test
 * @author zbw
 */
@RestController
@Slf4j
public class ControllerDemo {
    @GetMapping("/test")
    public void first(){
        log.info("test");
    }

}
