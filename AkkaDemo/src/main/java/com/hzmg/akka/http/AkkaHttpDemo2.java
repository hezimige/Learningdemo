package com.hzmg.akka.http;

import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.typed.javadsl.Behaviors;
import akka.cluster.Cluster;
import akka.http.javadsl.Http;
import akka.http.javadsl.ServerBuilder;
import akka.http.javadsl.server.AllDirectives;
import akka.http.javadsl.server.Route;
import akka.http.javadsl.server.directives.RouteAdapter;
import cn.hutool.core.lang.Console;
import com.google.common.collect.Maps;
import com.hzmg.akka.Aspect.RemoteServiceCreater;
import com.hzmg.akka.FriendActor;
import com.hzmg.akka.utils.NetUtils;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Component
@SpringBootApplication
public class AkkaHttpDemo2 extends AllDirectives {
    static final Logger logger = LoggerFactory.getLogger(AkkaHttpDemo2.class);
    static AkkaHttpDemo2 akkaHttpDemo = new AkkaHttpDemo2();

    public static void main(String[] args) throws Exception {
        akkaHttpDemo.startHttpServer();
        SpringApplication.run(AkkaHttpDemo2.class, args);


    }

    private void startHttpServer() {
        //server-bootstrap boot up server using the route as defined below
        /*Map<String, Object> overrideConfig = Maps.newHashMap();
        String localIP = NetUtils.getLocalHost();
        overrideConfig.put("akka.remote.artery.canonical.hostname", localIP);
        overrideConfig.put("akka.remote.artery.canonical.port", "10087");
        String actorSystemAddress = localIP + ":" + "10087";
        logger.info("[AkkaDemo] akka-remote server address: {}", actorSystemAddress);
        Config akkaBasicConfig = ConfigFactory.load("hzmg-server.akka.conf");
        Config akkaFinalConfig = ConfigFactory.parseMap(overrideConfig).withFallback(akkaBasicConfig);
        */
        ActorSystem actorSystem;
        //创建Actor工厂
        actorSystem = ActorSystem.create("remote-call");
        final Http http = Http.get(actorSystem);
        ServerBuilder serverBuilder=http.newServerAt(NetUtils.getLocalHost(),8888);
        logger.info("Server online at "+NetUtils.getLocalHost()+" 8888");
        logger.info("actorSystem ip:"+actorSystem.name());

        Route route2= path("hello1", () ->
                        get(() ->
                                complete("<h1>hello1 akka-http-demo</h1>")));
        Route route1=path("hello2", () ->
                get(() ->
                        complete("<h1>hello2 akka-http-demo</h1>")));
        List<Route> routes=new LinkedList<>();
        routes.add(route1);
        routes.add(route2);
        Route routess = createRoute();
        for (Route route:routes){
            routess=routess.orElse(route);
        }
        actorSystem.actorOf(Props.create(FriendActor.class),"friend");
        serverBuilder.bind(routess);

    }

    private Route createRoute() {
        return path("helloo", () ->
                        get(() ->
                                complete("<h1>hello akka-http-demo</h1>")));
    }
    private Route createRoute2() {
        return
                path("hello2", () ->
                        get(() ->
                                complete("<h1>hello2 akka-http-demo</h1>")));
    }

}
