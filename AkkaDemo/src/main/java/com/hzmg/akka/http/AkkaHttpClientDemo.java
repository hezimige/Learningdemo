package com.hzmg.akka.http;

import akka.actor.typed.ActorSystem;
import akka.actor.typed.javadsl.Behaviors;
import akka.http.javadsl.Http;
import akka.http.javadsl.model.HttpRequest;
import akka.http.javadsl.model.HttpResponse;
import com.hzmg.common.serialize.JsonUtils;

import java.util.concurrent.CompletionStage;

/**
 * this demo is the akka http client demo.
 */
public class AkkaHttpClientDemo {
    public static void main(String[] args) {
        ActorSystem<Void> system = ActorSystem.create(Behaviors.empty(), "httpClientDemo");
        final CompletionStage<HttpResponse> responseFuture =
                Http.get(system)
                        .singleRequest(HttpRequest.create("http://httpbin.org/cookies"));
        system.log().info(responseFuture.toString());
        //json序列化
        responseFuture.thenApply(s -> {
            System.out.println(JsonUtils.toJSONString(s.entity()));
            return null;
        });
        //自带函数返回
        responseFuture.whenComplete((response, throwable) -> {
            try {
                system.log().info(response.toString());
            } catch (Exception e) {
                system.log().error(throwable.toString());
            }
        });
    }
}
