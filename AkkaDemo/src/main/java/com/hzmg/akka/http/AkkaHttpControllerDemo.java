package com.hzmg.akka.http;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * this demo just a test which akka http can accept the springframework mvc web
 *
 */
@Controller
@ResponseBody
public class AkkaHttpControllerDemo {
    @GetMapping("/hello2")
    public String hello(){
        return "hello mvc for akka";
    }
}
