package com.hzmg.akka.service;

import com.hzmg.akka.Aspect.RemoteServiceCall;
import com.hzmg.akka.Aspect.RemoteServiceClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * remote service demo1
 *
 * @author zbw
 */
@RemoteServiceClass
public class RemoteCallServiceDemo1 {
    private static final Logger logger = LoggerFactory.getLogger(RemoteCallServiceDemo1.class);

    @RemoteServiceCall(name = "helloRemote")
    public String helloRemoteCall(@RequestBody String hi) {
        logger.debug("[RemoteCallServiceDemo1:]" + hi);
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "Hello ,this is remote service call!";
    }

}
