package com.hzmg.akka;

import com.hzmg.akka.utils.RemoteCallUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

/**
 * @author zbw
 */
@SpringBootApplication
public class UserRemoteCallUtilsDemo {
    private static Logger logger= LoggerFactory.getLogger(UserRemoteCallUtilsDemo.class);
    public static void main(String[] args){
        //SpringApplication.run(UserRemoteCallUtilsDemo.class, args);
        new SpringApplicationBuilder(UserRemoteCallUtilsDemo.class)
                .web(WebApplicationType.NONE)
                .run(args);
        //logger.debug((String) RemoteCallUtils.getRemoteService("hi remote server","helloRemote","remote-call"));
    }
}
