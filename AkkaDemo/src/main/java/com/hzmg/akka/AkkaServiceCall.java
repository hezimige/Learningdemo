package com.hzmg.akka;

import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import cn.hutool.core.lang.Console;
import com.hzmg.akka.model.TransportDemo1;

/**
 * 通过注解新建actor
 * 废案
 *
 * @author zbw
 */
public class AkkaServiceCall extends AbstractBehavior<Object> {
    private static String actorName = "test";

    public AkkaServiceCall(ActorContext<Object> context) {
        super(context);
        //新建一个actor
        getContext().spawn(AkkaServiceCall.create(), actorName);
    }

    static Behavior<Object> create() {
        Console.log("{} is created ",actorName);
        return Behaviors.setup(AkkaServiceCall::new);
    }

    @Override
    public Receive<Object> createReceive() {
        return newReceiveBuilder()
                .onMessage(TransportDemo1.class, this::onReceiveTransport)
                .onAnyMessage(obj -> {
                    getContext().getLog().info("[WorkerRequestAkkaHandler] receive unknown request: {}.", obj);
                    return this;
                })
                .build();
    }

    Behavior<Object> onReceiveTransport(TransportDemo1 transportDemo1) {
        Console.log("the test actor is accepting the message");
        TransportDemo1 transportDemo11 = new TransportDemo1();
        transportDemo11.setId(10);
        transportDemo11.setName("test");
        getContext().getSelf().tell(transportDemo11);
        getContext().getLog().info("接受消息并返回特定值");
        return Behaviors.same();
    }


}
