package com.hzmg.scheduled;

import org.junit.Test;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;


public class ScheduledExecutorTest {
    @Test
    /*
      带延迟时间的调度，只执行一次
      异步执行
      调度之后可通过Future.get()阻塞直至任务执行完毕
     */
    public void scheduleRunnableTest() throws Exception {
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        ScheduledFuture future = service.schedule(() -> {
            try {
                Thread.sleep(3000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("task finish time: " + System.currentTimeMillis());
        }, 1000, TimeUnit.MILLISECONDS);
        System.out.println("schedule finish time: " + System.currentTimeMillis());
        //future.get()用来获取执行的任务的返回值（如果有的话）这里没有设置返回值因此为空值。
        System.out.println("Runnnable future's result is: " + future.get() + ", and time is: " + System.currentTimeMillis());
    }
    @Test
    /*
      带延迟时间的调度，只执行一次
      调度之后可通过Future.get()阻塞直至任务执行完毕
     */
    public void scheduleCallbackTest() throws Exception {
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        ScheduledFuture future = service.schedule(() -> {
            try {
                Thread.sleep(3000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("task finish time: " + System.currentTimeMillis());
            return "success";
        }, 1000, TimeUnit.MILLISECONDS);
        System.out.println("schedule finish time: " + System.currentTimeMillis());
        //future.get()用来获取执行的任务的返回值（如果有的话）这里没有设置返回值因此为空值。
        System.out.println("Callback future's result is: " + future.get() + ", and time is: " + System.currentTimeMillis());
    }
    @Test
    /*
      固定频率对一个任务循环执行，相对于任务执行的开始时间
      可能导致上一个任务未执行完毕就开始下一个任务
      但最为精确
      初始延迟一秒，任务中执行3秒，执行间隔为一秒
     */
    public void scheduleAtFixedRateTest() throws Exception {
        //corePoolSize=5:维护的（守护的）线程数为五
        ScheduledExecutorService service = Executors.newScheduledThreadPool(5);
        service.scheduleAtFixedRate(() -> {
            try {
                Thread.sleep(3000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("task finish time: " + System.currentTimeMillis());
        }, 1000L, 1000L, TimeUnit.MILLISECONDS);

        System.out.println("schedule finish time: " + System.currentTimeMillis());
        //由于是异步执行该任务，为了保证该线程不会被结束从而能看到效果，使用一个while循环来保持线程存活。
        while (true) {
        }
    }
    @Test
    /*
      固定延迟对一个任务循环执行，相对于任务执行的结束时间
      精度不及固定频率
      初始延迟一秒，任务中执行3秒，执行间隔为一秒
     */
    public void scheduleWithFixedDelayTest() throws Exception {
        //corePoolSize=5:维护的（守护的）线程数为五
        ScheduledExecutorService service = Executors.newScheduledThreadPool(5);
        service.scheduleWithFixedDelay(() -> {
            try {
                Thread.sleep(3000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("task finish time: " + System.currentTimeMillis());
        }, 1000L, 1000L, TimeUnit.MILLISECONDS);

        System.out.println("schedule finish time: " + System.currentTimeMillis());
        //由于是异步执行该任务，为了保证该线程不会被结束从而能看到效果，使用一个while循环来保持线程存活。
        while (true) {
        }
    }
}
